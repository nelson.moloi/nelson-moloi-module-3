import 'package:flutter/material.dart';
import 'package:nelson_moloi_module_3/screens/authenticate/register.dart';
import 'package:nelson_moloi_module_3/screens/authenticate/login.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  //final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[400],
      appBar: AppBar(
        title: const Text("Kasi Chow"),
        centerTitle: true,
        backgroundColor: Colors.red[900],
        elevation: 0.0,
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 20.0),
              const Text('Welcome to Kasi Chow! Login / Register'),
              const SizedBox(height: 20.0),
              ElevatedButton(
                  child: const Text(
                    "Login",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const Login()),
                    );
                  },
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.red[900]))),
              const SizedBox(height: 20.0),
              ElevatedButton(
                  child: const Text(
                    "Register",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const Register()),
                    );
                  },
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.red[900]))),
            ],
          ),
        ),
      ),
    );
  }
}
